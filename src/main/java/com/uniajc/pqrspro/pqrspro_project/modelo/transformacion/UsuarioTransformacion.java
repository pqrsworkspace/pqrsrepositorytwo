package com.uniajc.pqrspro.pqrspro_project.modelo.transformacion;

import com.uniajc.pqrspro.pqrspro_project.modelo.Usuario;
import com.uniajc.pqrspro.pqrspro_project.modelo.dto.UsuarioDto;
import org.springframework.stereotype.Component;


@Component
public class UsuarioTransformacion {

    private UsuarioTransformacion() {

    }

    public  Usuario usuarioDtoToUsuario(UsuarioDto usuarioDtoSave) {

        return Usuario.builder()
                .id(usuarioDtoSave.getId())
                .nombre(usuarioDtoSave.getNombre())
                .apellido(usuarioDtoSave.getApellido())
                .correo(usuarioDtoSave.getCorreo())
                .username(usuarioDtoSave.getUsername())
                .password(usuarioDtoSave.getPassword())
                .rol(usuarioDtoSave.getRol())
                .build();
    }


}
