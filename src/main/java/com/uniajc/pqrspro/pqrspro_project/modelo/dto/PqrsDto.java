package com.uniajc.pqrspro.pqrspro_project.modelo.dto;

import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PqrsDto implements Serializable {

    private Long id;
    private Long idestudiante;
    private String nombreestudiante;
    private String area;
    private String descripcion;
    private String estado;
    private String correo;

    public boolean validarDto() {
        if (null == id) {
            return false;
        }
        return validarCampos();
    }

    private boolean validarCampos() {

        if (StringUtils.isBlank(nombreestudiante) || StringUtils.isBlank(area) || StringUtils.isBlank(correo)
                || StringUtils.isBlank(descripcion) || StringUtils.isBlank(estado) || null == idestudiante) {
            return false;
        }

        return validarCaracteres();
    }

    private boolean validarCaracteres() {

        String caracteresEspeciales = "|@&$</>";

        return !(StringUtils.containsAny(nombreestudiante, caracteresEspeciales) || StringUtils.containsAny(area, caracteresEspeciales)
                || StringUtils.containsAny(descripcion, caracteresEspeciales) || StringUtils.containsAny(estado, caracteresEspeciales));
    }

}
