package com.uniajc.pqrspro.pqrspro_project.modelo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "pqrs", schema = "public")
public class Pqrs {

    @Id
    @Column(name = "idpqrs")
    private Long id;

    @Column(name = "idestudiante")
    private Long idestudiante;

    @Column(name = "nombreestudiante")
    private String nombreestudiante;

    @Column(name = "area")
    private String area;

    @Column(name = "estado")
    private String estado;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "correo", nullable = false)
    private String correo;

}
