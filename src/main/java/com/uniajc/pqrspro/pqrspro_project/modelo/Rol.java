package com.uniajc.pqrspro.pqrspro_project.modelo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "rol", schema = "public")
public class Rol {

    @Id
    @Column(name = "idrol")
    private Long idrol;

    @Column(name = "nombrerol")
    private String nombrerol;


}
