package com.uniajc.pqrspro.pqrspro_project.modelo.transformacion;

import com.uniajc.pqrspro.pqrspro_project.modelo.Pqrs;
import com.uniajc.pqrspro.pqrspro_project.modelo.dto.PqrsDto;
import org.springframework.stereotype.Component;

@Component
public class PqrsTransformacion {

    public Pqrs pqrsDtoToPqrs(PqrsDto pqrsDtoSave) {

        return Pqrs.builder()
                .id(pqrsDtoSave.getId())
                .idestudiante(pqrsDtoSave.getIdestudiante())
                .nombreestudiante(pqrsDtoSave.getNombreestudiante())
                .area(pqrsDtoSave.getArea())
                .descripcion(pqrsDtoSave.getDescripcion())
                .estado(pqrsDtoSave.getEstado())
                .correo(pqrsDtoSave.getCorreo())
                .build();
    }


}
