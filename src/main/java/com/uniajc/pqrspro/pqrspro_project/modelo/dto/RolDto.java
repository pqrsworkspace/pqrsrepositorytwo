package com.uniajc.pqrspro.pqrspro_project.modelo.dto;

import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;


@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RolDto implements Serializable {

    private Long idrol;
    private String nombrerol;

    public boolean validarDto() {

        if (null == idrol) {
            return false;
        }
        return validarCampos();
    }

    private boolean validarCampos() {

        if (StringUtils.isBlank(nombrerol)) {
            return false;
        }
        return validarCaracteres();

    }

    private boolean validarCaracteres() {

        String caracteresEspeciales = "|@&$</>";

        return !StringUtils.containsAny(nombrerol, caracteresEspeciales);

    }


}
