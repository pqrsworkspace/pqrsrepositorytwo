package com.uniajc.pqrspro.pqrspro_project.modelo.transformacion;

import com.uniajc.pqrspro.pqrspro_project.modelo.Rol;
import com.uniajc.pqrspro.pqrspro_project.modelo.dto.RolDto;
import org.springframework.stereotype.Component;

@Component
public class RolTransformacion {

    private RolTransformacion() {

    }

    public  Rol rolDtoToRol(RolDto rolDtoSave) {

        return Rol.builder()
                .idrol(rolDtoSave.getIdrol())
                .nombrerol(rolDtoSave.getNombrerol())
                .build();
    }
}
