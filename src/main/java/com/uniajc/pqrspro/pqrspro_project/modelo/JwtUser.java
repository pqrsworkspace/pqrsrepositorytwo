package com.uniajc.pqrspro.pqrspro_project.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JwtUser
{
    private String userName;
    private String role;

}
