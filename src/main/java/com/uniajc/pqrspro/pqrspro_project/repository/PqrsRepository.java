package com.uniajc.pqrspro.pqrspro_project.repository;

import com.uniajc.pqrspro.pqrspro_project.modelo.Pqrs;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PqrsRepository extends CrudRepository<Pqrs, Long> {

    List<Pqrs> findAllByIdestudiante(Long id);

    List<Pqrs> findAllByArea(String area);

    List<Pqrs> findAllByEstado(String estado);
}
