package com.uniajc.pqrspro.pqrspro_project.service;

import com.uniajc.pqrspro.pqrspro_project.modelo.Usuario;
import com.uniajc.pqrspro.pqrspro_project.modelo.dto.UsuarioDto;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface UsuarioService {

    Usuario loguear(UsuarioDto usuarioDto) throws NoSuchAlgorithmException;

    Usuario cambiarClave(UsuarioDto usuarioDto) throws NoSuchAlgorithmException;

    String registrar(UsuarioDto usuarioDto);

    String actualizar(UsuarioDto usuarioDto);

    String eliminar(Long id);

    Usuario consultarUsuarioById(Long id);

    List<Usuario> consultarUsuarios();

}
