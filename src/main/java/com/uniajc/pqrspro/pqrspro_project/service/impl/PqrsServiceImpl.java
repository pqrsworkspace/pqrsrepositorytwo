package com.uniajc.pqrspro.pqrspro_project.service.impl;

import com.uniajc.pqrspro.pqrspro_project.exception.PqrsException;
import com.uniajc.pqrspro.pqrspro_project.modelo.Pqrs;
import com.uniajc.pqrspro.pqrspro_project.modelo.dto.PqrsDto;
import com.uniajc.pqrspro.pqrspro_project.modelo.transformacion.PqrsTransformacion;
import com.uniajc.pqrspro.pqrspro_project.repository.PqrsRepository;
import com.uniajc.pqrspro.pqrspro_project.service.PqrsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PqrsServiceImpl implements PqrsService {

    @Autowired
    private PqrsRepository pqrsRepository;

    @Autowired
    private PqrsTransformacion pqrsTransformacion;

    @Override
    public String registrar(PqrsDto pqrsDtoSave) {

        if (pqrsDtoSave.validarDto()){
            Pqrs dtoPqrsSave = pqrsTransformacion.pqrsDtoToPqrs(pqrsDtoSave);
            pqrsRepository.save(dtoPqrsSave);
            return "Registro Exitoso";
        }
        throw new PqrsException("Los campos no cumplen las condiciones");
    }

    @Override
    public String actualizar(PqrsDto pqrsDtoUpdate) {

        if (pqrsDtoUpdate.validarDto()) {
            Pqrs dtoPqrsUpdate = pqrsTransformacion.pqrsDtoToPqrs(pqrsDtoUpdate);
           pqrsRepository.save(dtoPqrsUpdate);
           return "Registro Exitoso";
       }
        throw new PqrsException("Los campos no cumplen las condiciones");

    }

    @Override
    public String eliminar(Long id) {
        if (pqrsRepository.existsById(id)){
            pqrsRepository.deleteById(id);
            return "Eliminado con Exito";
        }
        throw new PqrsException("Ocurrio un error al eliminar, el id no existe en la base de datos");
    }

    @Override
    public Pqrs consultarPqrsById(Long id) {
        return pqrsRepository.findById(id).orElse(null);
    }

    @Override
    public List<Pqrs> consultarPqrsByIdEstudiante(Long id) {
        return pqrsRepository.findAllByIdestudiante(id);
    }

    @Override
    public List<Pqrs> consultarPqrsByArea(String area) {
            return pqrsRepository.findAllByArea(area);
    }

    @Override
    public List<Pqrs> consultarPqrsByEstado(String estado) {
        return pqrsRepository.findAllByEstado(estado);

    }

    @Override
    public List<Pqrs> consultarPqrs() {
        return (List<Pqrs>) pqrsRepository.findAll();
    }
}
