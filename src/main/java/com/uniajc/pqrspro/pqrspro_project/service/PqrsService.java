package com.uniajc.pqrspro.pqrspro_project.service;

import com.uniajc.pqrspro.pqrspro_project.modelo.Pqrs;
import com.uniajc.pqrspro.pqrspro_project.modelo.dto.PqrsDto;

import java.util.List;

public interface PqrsService {

    String registrar(PqrsDto pqrsDto);

    String actualizar(PqrsDto pqrsDto);

    String eliminar(Long id);

    Pqrs consultarPqrsById(Long id);

    List<Pqrs> consultarPqrsByIdEstudiante(Long id);

    List<Pqrs> consultarPqrsByArea(String area);

    List<Pqrs> consultarPqrsByEstado(String estado);

    List<Pqrs> consultarPqrs();
}
