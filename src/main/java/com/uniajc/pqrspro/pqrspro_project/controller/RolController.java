package com.uniajc.pqrspro.pqrspro_project.controller;

import com.uniajc.pqrspro.pqrspro_project.modelo.dto.GenericDto;
import com.uniajc.pqrspro.pqrspro_project.modelo.dto.RolDto;
import com.uniajc.pqrspro.pqrspro_project.service.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/Rol")


public class RolController {

    @Autowired
    private RolService rolService;

    @PostMapping("/registrar")

    public ResponseEntity<GenericDto> registrar(@RequestBody RolDto rolDto) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.rolService.registrar(rolDto)));
    }

    @PutMapping("/actualizar")

    public ResponseEntity<GenericDto> actualizar(@RequestBody RolDto rolDto) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.rolService.actualizar(rolDto)));
    }

    @DeleteMapping("/eliminar")

    public ResponseEntity<GenericDto> eliminar(@RequestParam("id") Long id) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.rolService.eliminar(id)));
    }

    @GetMapping("/consultarRolById")
    public ResponseEntity<GenericDto> consultarRolById(@RequestParam("id") Long id) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.rolService.consultarRolById(id)));
    }

    @GetMapping("/consultarRoles")
    public ResponseEntity<GenericDto> consultarRoles() {
        return ResponseEntity.ok().body(GenericDto.sucess(this.rolService.consultarRoles()));
    }
}
