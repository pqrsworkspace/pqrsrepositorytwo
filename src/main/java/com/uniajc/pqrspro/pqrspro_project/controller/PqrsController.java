package com.uniajc.pqrspro.pqrspro_project.controller;

import com.uniajc.pqrspro.pqrspro_project.modelo.dto.GenericDto;
import com.uniajc.pqrspro.pqrspro_project.modelo.dto.PqrsDto;
import com.uniajc.pqrspro.pqrspro_project.service.PqrsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/Pqrs")


public class PqrsController {

    @Autowired
    private PqrsService pqrsServices;

    @PostMapping("/registrar")

    public ResponseEntity<GenericDto> registrar(@RequestBody PqrsDto pqrsDto) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.pqrsServices.registrar(pqrsDto)));
    }

    @PutMapping("/actualizar")

    public ResponseEntity<GenericDto> actualizar(@RequestBody PqrsDto pqrsDto) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.pqrsServices.actualizar(pqrsDto)));
    }

    @DeleteMapping("/eliminar")

    public ResponseEntity<GenericDto> eliminar(@RequestParam("id") Long id) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.pqrsServices.eliminar(id)));
    }

    @GetMapping("/id")
    public ResponseEntity<GenericDto> consultarPqrsById(@RequestParam("id") Long id) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.pqrsServices.consultarPqrsById(id)));
    }

    @GetMapping("/all/estudiante")
    public ResponseEntity<GenericDto> consultarPqrsByIdEstudiante(@RequestParam("idestudiante") Long id) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.pqrsServices.consultarPqrsByIdEstudiante(id)));
    }

    @GetMapping("/all/area")
    public ResponseEntity<GenericDto> consultarPqrsByArea(@RequestParam("area") String area) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.pqrsServices.consultarPqrsByArea(area)));
    }

    @GetMapping("/all/estado")
    public ResponseEntity<GenericDto> consultarPqrsByEstado(@RequestParam("estado") String estado) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.pqrsServices.consultarPqrsByEstado(estado)));
    }

    @GetMapping("/all")
    public ResponseEntity<GenericDto> consultarPqrs() {
        return ResponseEntity.ok().body(GenericDto.sucess(this.pqrsServices.consultarPqrs()));
    }
}
