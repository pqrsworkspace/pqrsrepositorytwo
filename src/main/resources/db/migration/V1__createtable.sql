CREATE TABLE pqrs (
	idpqrs int4 NULL,
	idestudiante int4 NULL,
	nombreestudiante varchar(40) NULL,
	area varchar(30) NULL,
	descripcion varchar(1000) NULL,
	estado varchar(2) NULL,
	correo varchar(50) NULL
);
CREATE TABLE rol (
	idrol int8 NOT NULL,
nombrerol varchar(100) NOT NULL,
CONSTRAINT idrol PRIMARY KEY (idrol)
);
CREATE TABLE usuario (
	id int4 NOT NULL,
	nombre varchar(100) NOT NULL,
	apellido varchar(100) NOT NULL,
	correo varchar(200) NOT NULL,
	"username" varchar(100) NULL,
	"password" varchar(150) NULL,
	rol int8 NOT NULL
);