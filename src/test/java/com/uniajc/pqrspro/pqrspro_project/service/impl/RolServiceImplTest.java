package com.uniajc.pqrspro.pqrspro_project.service.impl;

import com.uniajc.pqrspro.pqrspro_project.exception.PqrsException;
import com.uniajc.pqrspro.pqrspro_project.modelo.Rol;
import com.uniajc.pqrspro.pqrspro_project.modelo.dto.RolDto;
import com.uniajc.pqrspro.pqrspro_project.service.RolService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class RolServiceImplTest {

    @Autowired
    RolService rolService;

    @DisplayName(value = "test Rol -> la respuesta de la consulta trae el listado de roles")
    @Test
    void testConsutarRoles() {
        List<Rol> resultReal = rolService.consultarRoles();
        assertThat(resultReal.size()).isPositive();
    }

    @DisplayName(value = "test Rol -> consultar roles por id")
    @Test
    void testConsutarRolesId() {
        Rol resultReal = rolService.consultarRolById(3L);
        assertThat(resultReal).isNotNull();
    }

    @DisplayName(value = "test Auth -> Cuando se crea un rol de forma correcta.")
    @Test
    void testForm() {

        RolDto rolDto = new RolDto();

        rolDto.setIdrol(3L);
        rolDto.setNombrerol("Administrador");
        String respuestaEsperada = "Registro Exitoso";
        String respuestaOptenida = null;

            respuestaOptenida = rolService.registrar(rolDto);

        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un rol de forma correcta.")
    @Test
    void testForm2() {

        RolDto rolDto = new RolDto();

        rolDto.setIdrol(5L);
        rolDto.setNombrerol("Administrador");
        String respuestaEsperada = "Registro Exitoso";
        String respuestaOptenida = null;

            respuestaOptenida = rolService.registrar(rolDto);

        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se actualiza un rol de forma correcta.")
    @Test
    void testUpdateForm() {

        RolDto rolDto = new RolDto();

        rolDto.setIdrol(5L);
        rolDto.setNombrerol("Estudiante");
        String respuestaEsperada = "Registro Exitoso";
        String respuestaOptenida = null;
        respuestaOptenida = rolService.actualizar(rolDto);
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un rol y se envia null el formulario.")
    @Test
    void testNullForm() {
        RolDto rolDto = new RolDto();
        rolDto.setIdrol(null);
        rolDto.setNombrerol(null);
        Exception exception = assertThrows(PqrsException.class,()->rolService.registrar(rolDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un rol y se envia null el id.")
    @Test
    void testNullId() {
        RolDto rolDto = new RolDto();
        rolDto.setIdrol(null);
        rolDto.setNombrerol("Estudiante");
        Exception exception = assertThrows(PqrsException.class,()->rolService.registrar(rolDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un rol y se envia null el nombrerol.")
    @Test
    void testNullName() {
        RolDto rolDto = new RolDto();
        rolDto.setIdrol((long) 2);
        rolDto.setNombrerol(null);
        Exception exception = assertThrows(PqrsException.class,()->rolService.registrar(rolDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un rol y se envia vacio el nombre.")
    @Test
    void testEmptyName() {
        RolDto rolDto = new RolDto();
        rolDto.setIdrol((long) 2);
        rolDto.setNombrerol(" ");
        Exception exception = assertThrows(PqrsException.class,()->rolService.registrar(rolDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un rol y no se envia un caracter en el nombre.")
    @Test
    void testCharacterName() {
        RolDto rolDto = new RolDto();
        rolDto.setIdrol((long) 2);
        rolDto.setNombrerol("/*<>");
        Exception exception = assertThrows(PqrsException.class,()->rolService.registrar(rolDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se actualiza un rol y no se envia nul en el formulario.")
    @Test
    void testUpdateNullForm() {
        RolDto rolDto = new RolDto();
        rolDto.setIdrol(null);
        rolDto.setNombrerol(null);
        Exception exception = assertThrows(PqrsException.class,()->rolService.actualizar(rolDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se actualiza un rol y no se envia null en el nombre.")
    @Test
    void testUpdateNullName() {
        RolDto rolDto = new RolDto();
        rolDto.setIdrol((long) 2);
        rolDto.setNombrerol(null);
        Exception exception = assertThrows(PqrsException.class,()->rolService.actualizar(rolDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = " test Auth -> cuando se intenta eliminar un id que no existe")
    @Test
    void testDeleteNotId(){
        Exception exception = assertThrows(PqrsException.class,()->rolService.eliminar(1000000L));
        String respuestaEsperada = "Ocurrio un error al eliminar, el id no existe en la base de datos";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = " test Auth -> cuando se elimina un id")
    @Test
    void testDeleteId() {
        String respuestaEsperada = "Eliminado con exito";
        String respuestaOptenida = rolService.eliminar(5L);
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

}