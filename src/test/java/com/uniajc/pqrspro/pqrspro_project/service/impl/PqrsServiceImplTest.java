package com.uniajc.pqrspro.pqrspro_project.service.impl;

import com.uniajc.pqrspro.pqrspro_project.exception.PqrsException;
import com.uniajc.pqrspro.pqrspro_project.modelo.Pqrs;
import com.uniajc.pqrspro.pqrspro_project.modelo.dto.PqrsDto;
import com.uniajc.pqrspro.pqrspro_project.service.PqrsService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class PqrsServiceImplTest {

    @Autowired
    PqrsService pqrsService;

    @DisplayName(value = "test Pqrs -> que la respuesta de la consulta trae el listado pqrs.")
    @Test
    void testConsultarPqrs() {
        List<Pqrs> resultReal = pqrsService.consultarPqrs();
        assertThat(resultReal.size()).isPositive();
    }
    @DisplayName(value = "test Pqrs -> consultar roles por id")
    @Test
    void testConsutarPqrsId() {
        Pqrs resultReal = pqrsService.consultarPqrsById(3L);
        assertThat(resultReal).isNotNull();
    }

    @DisplayName(value = "test Pqrs -> consultar roles por id")
    @Test
    void testConsutarPqrsIdStudent() {
       List<Pqrs>  resultReal = pqrsService.consultarPqrsByIdEstudiante(2L);
        assertThat(resultReal.size()).isPositive();
    }

    @DisplayName(value = "test Pqrs -> consultar pqrs por estado")
    @Test
    void testConsultPqrsByState() {
        List<Pqrs> resultReal = pqrsService.consultarPqrsByEstado("A");
        assertThat(resultReal.size()).isPositive();
    }

    @DisplayName(value = "test Pqrs -> consultar pqrs por estado")
    @Test
    void testConsultPqrsByArea() {
        List<Pqrs> resultReal = pqrsService.consultarPqrsByArea("TIC");
        assertThat(resultReal.size()).isPositive();
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs de la forma correcta.")
    @Test
    void testForm() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId(3L);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("cristhian");
        pqrsDto.setArea("Tic");
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        String respuestaEsperada = "Registro Exitoso";
        String respuestaOptenida = pqrsService.registrar(pqrsDto);
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia null el formulario .")
    @Test
    void testNullForm() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId(null);
        pqrsDto.setIdestudiante(null);
        pqrsDto.setNombreestudiante(null);
        pqrsDto.setArea(null);
        pqrsDto.setDescripcion(null);
        pqrsDto.setEstado(null);
        pqrsDto.setCorreo(null);
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia null el id .")
    @Test
    void testNullId() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId(null);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("cristhian");
        pqrsDto.setArea("Tic");
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia null el idEstudiante.")
    @Test
    void testNullIdStudent() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 3);
        pqrsDto.setIdestudiante(null);
        pqrsDto.setNombreestudiante("cristhian");
        pqrsDto.setArea("Tic");
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia null el nombreEstudiante.")
    @Test
    void testNullNameStudent() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 3);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante(null);
        pqrsDto.setArea("Tic");
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia null el area.")
    @Test
    void testNullArea() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 3);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("Cristhian");
        pqrsDto.setArea(null);
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia null el la descripcion.")
    @Test
    void testNullDescription() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 3);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("Cristhian");
        pqrsDto.setArea("TIC");
        pqrsDto.setDescripcion(null);
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia null el estado.")
    @Test
    void testNullState() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 3);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("Cristhian");
        pqrsDto.setArea("TIC");
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado(null);
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia null el email.")
    @Test
    void testNullEmail() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 3);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("Cristhian");
        pqrsDto.setArea("TIC");
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo(null);
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia un caracter en el nombreEstudiante.")
    @Test
    void testCharacterNameStudent() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 3);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("/*<>");
        pqrsDto.setArea("TIC");
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia caracter en el area.")
    @Test
    void testCharacterArea() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 3);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("juan");
        pqrsDto.setArea("<>/*");
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia caracter en la descripcion.")
    @Test
    void testCharacterDescription() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 3);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("Cristhian");
        pqrsDto.setArea("TIC");
        pqrsDto.setDescripcion("</*@>");
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia caracter en el estado.")
    @Test
    void testCharacterState() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 3);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("Cristhian");
        pqrsDto.setArea("TIC");
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado("<>*/");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }


    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia vacio el formulario.")
    @Test
    void testEmptyForm() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 3);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante(" ");
        pqrsDto.setArea(" ");
        pqrsDto.setDescripcion(" ");
        pqrsDto.setEstado(" ");
        pqrsDto.setCorreo(" ");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia vacio el nombreEstudiante.")
    @Test
    void testEmptyNameStudent() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 3);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante(" ");
        pqrsDto.setArea("TIC");
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia vacio el area.")
    @Test
    void testEmptyArea() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 3);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("cristhian");
        pqrsDto.setArea(" ");
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia vacio la description.")
    @Test
    void testEmptyDescription() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 3);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("cristhian");
        pqrsDto.setArea("TIC");
        pqrsDto.setDescripcion(" ");
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia vacio el estado.")
    @Test
    void testEmptyState() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 3);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("cristhian");
        pqrsDto.setArea("TIC");
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado(" ");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia vacio el email.")
    @Test
    void testEmptyEmail() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 3);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("cristhian");
        pqrsDto.setArea("TIC");
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo(" ");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.registrar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se actualiza una pqrs de la forma correcta.")
    @Test
    void testUpdateForm() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 1);
        pqrsDto.setIdestudiante((long) 1);
        pqrsDto.setNombreestudiante("cristhian");
        pqrsDto.setArea("Tic");
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        String respuestaEsperada = "Registro Exitoso";
        String respuestaOptenida = pqrsService.actualizar(pqrsDto);
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia nul el formulario.")
    @Test
    void testUpdateNullForm() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId(null);
        pqrsDto.setIdestudiante(null);
        pqrsDto.setNombreestudiante(null);
        pqrsDto.setArea(null);
        pqrsDto.setDescripcion(null);
        pqrsDto.setEstado(null);
        pqrsDto.setCorreo(null);
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.actualizar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envia vacio el formulario.")
    @Test
    void testUpdateEmptyForm() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 2);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante(" ");
        pqrsDto.setArea(" ");
        pqrsDto.setDescripcion(" ");
        pqrsDto.setEstado(" ");
        pqrsDto.setCorreo(" ");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.actualizar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs y se envian caracteres en el formulario.")
    @Test
    void testUpdateCharactersForm() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId((long) 2);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("/*<>@");
        pqrsDto.setArea("/*<>@");
        pqrsDto.setDescripcion("/*<>@");
        pqrsDto.setEstado("/*<>@");
        pqrsDto.setCorreo("/*<>@");
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.actualizar(pqrsDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = " test Auth -> cuando se intenta eliminar un id que no existe")
    @Test
    void testDeleteNotId(){
        Exception exception = assertThrows(PqrsException.class,()->pqrsService.eliminar(1000000L));
        String respuestaEsperada = "Ocurrio un error al eliminar, el id no existe en la base de datos";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = " test Auth -> cuando se elimina un id")
    @Test
    void testDeleteId(){
        String respuestaEsperada = "Eliminado con Exito";
        String respuestaOptenida = pqrsService.eliminar(1L);
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }
}