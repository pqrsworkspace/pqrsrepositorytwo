package com.uniajc.pqrspro.pqrspro_project.controller;

import com.uniajc.pqrspro.pqrspro_project.modelo.dto.PqrsDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
class PqrsControllerTest {

    @Autowired
    PqrsController pqrsController;

    @DisplayName(value = "test Auth -> Cuando se crea una pqrs de la forma correcta.")
    @Test
    void testFormController() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId(1L);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("cristhian");
        pqrsDto.setArea("Tic");
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        ResponseEntity respuestaOptenida = pqrsController.registrar(pqrsDto);
        assertThat(respuestaOptenida).isNotNull();
    }

    @DisplayName(value = "test Auth -> Cuando se actualiza una pqrs de la forma correcta.")
    @Test
    void testUpdateFormController() {

        PqrsDto pqrsDto = new PqrsDto();

        pqrsDto.setId(100L);
        pqrsDto.setIdestudiante((long) 2);
        pqrsDto.setNombreestudiante("cristhian");
        pqrsDto.setArea("Tic");
        pqrsDto.setDescripcion("LOS COMPUTADORES DE LA SALA 103 PRESENTA UN PROBLEMA DE LENTITUD COMO INSTUTICION DEBERIAN GARANTIZAR EL FUNCIONAMIENTO OPTIMO DE LOS EQUIPOS");
        pqrsDto.setEstado("A");
        pqrsDto.setCorreo("cpatarroyo@gmail.com");
        ResponseEntity respuestaOptenida = pqrsController.actualizar(pqrsDto);
        assertThat(respuestaOptenida).isNotNull();
    }

    @DisplayName(value = " test Auth -> cuando se elimina un id")
    @Test
    void testDeleteId(){
        ResponseEntity respuestaOptenida = pqrsController.eliminar(1L);
        assertThat(respuestaOptenida).isNotNull();
    }

    @DisplayName(value = " test Auth -> cuando se Consultan todas las pqrs")
    @Test
    void testConsultarPqrs() {
        ResponseEntity resultReal = pqrsController.consultarPqrs();
        assertThat(resultReal).isNotNull();
    }

    @DisplayName(value = "test Pqrs -> consultar roles por id")
    @Test
    void testConsutarPqrsId() {
        ResponseEntity resultReal = pqrsController.consultarPqrsById(3L);
        assertThat(resultReal).isNotNull();
    }

    @DisplayName(value = "test Pqrs -> consultar roles por id")
    @Test
    void testConsutarPqrsIdStudent() {
        ResponseEntity resultReal = pqrsController.consultarPqrsByIdEstudiante(2L);
        assertThat(resultReal).isNotNull();
    }

    @DisplayName(value = "test Pqrs -> consultar pqrs por estado")
    @Test
    void testConsultPqrsByState() {
        ResponseEntity resultReal = pqrsController.consultarPqrsByEstado("A");
        assertThat(resultReal).isNotNull();
    }

    @DisplayName(value = "test Pqrs -> consultar pqrs por estado")
    @Test
    void testConsultPqrsByArea() {
        ResponseEntity resultReal = pqrsController.consultarPqrsByArea("TIC");
        assertThat(resultReal).isNotNull();
    }


}